package org.misha.autocommit;

import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.consumer.OffsetCommitCallback;
import org.apache.kafka.common.TopicPartition;
import org.misha.Trace;
import org.misha.serialization.TraceDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.Collections.singletonList;

public class TracerConsumerWithReBalanceListener {
    private static final Logger log = LoggerFactory.getLogger(TracerConsumerWithReBalanceListener.class);

    public static void main(String[] args) {
        final AtomicLong lastTimestamp = new AtomicLong();
        Properties props = makeProperties();
        Map<TopicPartition, OffsetAndMetadata> currentOffsets = new HashMap<>();//should be some persistent storage
        try (KafkaConsumer<String, Trace> consumer = new KafkaConsumer<>(props)) {
            //commit offsets into the custom storage before re-balance
            class ReBalanceHandler implements ConsumerRebalanceListener {
                @Override
                public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
                    consumer.commitSync(currentOffsets);
                }

                @Override
                public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
                }
            }
            consumer.subscribe(singletonList("TracingPartitionedTopic"), new ReBalanceHandler());
            int count;
            while (true) {
                ConsumerRecords<String, Trace> records = consumer.poll(Duration.ZERO);
                count = 0;
                for (ConsumerRecord<String, Trace> record : records) {
                    if (lastTimestamp.get() < record.timestamp()) {
                        lastTimestamp.set(record.timestamp());
                        processData(record);
                        TopicPartition topicPartition = new TopicPartition(record.topic(), record.partition());
                        OffsetAndMetadata offsetAndMetadata = new OffsetAndMetadata(record.offset() + 1);
                        currentOffsets.put(topicPartition, offsetAndMetadata);
                        if (count % 10 == 0) {//commit offsets in ordinary situation
                            consumer.commitAsync(currentOffsets, //not retries
                                    new OffsetCommitCallback() {
                                        @Override
                                        public void onComplete(Map<TopicPartition, OffsetAndMetadata> offsets,
                                                               Exception exception) {
                                            if (exception != null) {
                                                log.error("Commit failed with offset: " + offsets);
                                            }
                                        }
                                    });
                        }
                        count++;
                    }
                }
            }
        }
    }

    private static void processData(ConsumerRecord<String, Trace> record) {
        log.info("\n=======================" +
                        "\nrecord: headers={}" +
                        "\nTrack is={}" +
                        "\nPARTITION={}" +
                        "\ntopic={}" +
                        "\ntimestamp={}" +
                        "\noffset={}" +
                        "\nlatitude={}" +
                        "\nlongitude={}",
                record.headers(),
                record.key(),
                record.partition(),
                record.topic(),
                record.timestamp(),
                record.offset(),
                record.value().getLatitude(),
                record.value().getLongitude());
    }

    private static Properties makeProperties() {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.deserializer", org.apache.kafka.common.serialization.StringDeserializer.class.getName());
        props.put("value.deserializer", TraceDeserializer.class.getName());
        props.put("group.id", "TraceTopicId");
        //props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        //props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "500");
        props.put("auto.commit.offset", "false");
        return props;
    }
}
