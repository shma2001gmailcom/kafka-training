package org.misha.autocommit;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.clients.consumer.OffsetCommitCallback;
import org.apache.kafka.common.TopicPartition;
import org.misha.Trace;
import org.misha.serialization.TraceDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.GROUP_ID_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG;

public class TracerConsumerWithCustomOffsetCommit {
    private static final Logger log = LoggerFactory.getLogger(TracerConsumerWithCustomOffsetCommit.class);

    public static void main(String[] args) {
        final AtomicLong lastTimestamp = new AtomicLong();
        Properties props = makeProperties();
        try (KafkaConsumer<String, Trace> consumer = new KafkaConsumer<>(props)) {
            consumer.subscribe(singletonList("TracingPartitionedTopic"));
            int count;
            while (true) {
                ConsumerRecords<String, Trace> records = consumer.poll(Duration.ZERO);
                count = 0;
                for (ConsumerRecord<String, Trace> record : records) {
                    if (lastTimestamp.get() < record.timestamp()) {
                        lastTimestamp.set(record.timestamp());
                        log.info(String.format("\n=======================\nrecord: headers=%%%%s\nTrack " +
                                        "is=%%%%s\nPARTITION=%%%%s\ntopic=%%%%s\ntimestamp=%%%%s\nlatitude=%%%%s" +
                                        "\nlongitude=%%%%s"),
                                record.headers(),
                                record.key(),
                                record.partition(),
                                record.topic(),
                                record.timestamp(),
                                record.value().getLatitude(),
                                record.value().getLongitude());
                        if (count % 10 == 0) {
                            TopicPartition topicPartition = new TopicPartition(record.topic(), record.partition());
                            OffsetAndMetadata offsetAndMetadata = new OffsetAndMetadata(record.offset() + 1);
                            consumer.commitAsync(singletonMap(topicPartition, offsetAndMetadata),
                                    new OffsetCommitCallback() {
                                        @Override
                                        public void onComplete(Map<TopicPartition, OffsetAndMetadata> offsets,
                                                               Exception exception) {
                                            if (exception != null) {
                                                log.error("Commit failed with offset: " + offsets);
                                            }
                                        }
                                    });
                        }
                        count++;
//                        consumer.commitAsync(new OffsetCommitCallback() {
//                            @Override
//                            public void onComplete(Map<TopicPartition, OffsetAndMetadata> offsets, Exception
//                            exception) {
//                                if (exception != null) {
//                                    System.out.println("Commit failed with offset: " + offsets);
//                                }
//                            }
//                        });//not retries ;
                        //consumer.commitSync(); //retries
                    }
                }
            }
        }
    }

    private static Properties makeProperties() {
        Properties props = new Properties();
        props.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(KEY_DESERIALIZER_CLASS_CONFIG,
                org.apache.kafka.common.serialization.StringDeserializer.class.getName());
        props.put(VALUE_DESERIALIZER_CLASS_CONFIG, TraceDeserializer.class.getName());
        props.put(GROUP_ID_CONFIG, "TraceTopicId");
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        //props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "500");
        props.put("auto.commit.offset", "false");
        return props;
    }
}