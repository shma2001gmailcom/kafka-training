package org.misha.transactions;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.misha.Trace;
import org.misha.serialization.TraceDeserializer;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.stream.IntStream;

import static org.apache.kafka.clients.producer.ProducerConfig.*;
import static org.misha.Utils.round;
import static org.misha.Utils.writeJson;

public class TracerProducer {

    public static void main(String[] args) {
        Properties props = makeProperties();
        try (KafkaProducer<String, Trace> producer = new KafkaProducer<>(props)) {
            producer.initTransactions();
            IntStream.range(0, 1000).mapToObj(t -> new Trace("TRACK_0", round(51.51101 + t * (51.50757 - 51.51101) / 1000),
                    round(-0.07620 + t * (-0.12387 - (-0.07620)) / 1000))).forEach(trace -> {
                producer.beginTransaction();
                ProducerRecord<String, Trace> record = new ProducerRecord<>("TracingPartitionedTopic", "Trace_Track_" + System.currentTimeMillis(), trace);
                try {
                    RecordMetadata recordMetadata = producer.send(record, (metadata, exception) -> {
                        if (exception != null) {
                            throw new RuntimeException(writeJson(record) + "\n" + exception);
                        }
                        System.err.println("Successfully sent: " + trace + "\nmeta:" + metadata);
                    }).get();
                    producer.commitTransaction();
                } catch (InterruptedException e) {
                    producer.abortTransaction();
                    Thread.currentThread().interrupt();
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    producer.abortTransaction();
                    throw new RuntimeException(e);
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static Properties makeProperties() {
        Properties props = new Properties();
        props.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(KEY_SERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.StringDeserializer.class.getName());
        props.put(VALUE_SERIALIZER_CLASS_CONFIG, TraceDeserializer.class.getName());
        props.put(TRANSACTIONAL_ID_CONFIG, "trace-producer-one");
        props.put(MAX_BLOCK_MS_CONFIG, "1000");
        return props;
    }
}
