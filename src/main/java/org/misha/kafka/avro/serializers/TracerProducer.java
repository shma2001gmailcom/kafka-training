package org.misha.kafka.avro.serializers;

import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.misha.kafka.avro.Trace;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.stream.IntStream;

import static org.misha.Utils.round;
import static org.misha.Utils.writeJson;

//pushes schema into registry
public class TracerProducer {

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.serializer", KafkaAvroSerializer.class.getName());
        props.put("value.serializer", KafkaAvroSerializer.class.getName());
        props.put("schema.registry.url", "http://localhost:8081");
        try (KafkaProducer<String, Trace> producer = new KafkaProducer<>(props)) {
            IntStream.range(0, 1000).mapToObj(t -> new Trace("TRACK_AVRO_SERIALIZED",
                    round(51.51101 + t * (51.50757 - 51.51101) / 1000).doubleValue(),
                    round(-0.07620 + t * (-0.12387 - (-0.07620)) / 1000).doubleValue())).forEach(trace -> {
                ProducerRecord<String, Trace> record
                        = new ProducerRecord<>("TraceAvroTopic", "Trace_Track_0", trace);
                try {
                    RecordMetadata recordMetadata = producer.send(record, (metadata, exception) -> {
                        if (exception != null) {
                            throw new RuntimeException(writeJson(record));
                        }
                        System.err.println("Successfully sent: " + trace + "\nmeta:" + metadata);
                    }).get();
                    System.err.println(recordMetadata);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
