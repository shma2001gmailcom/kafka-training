package org.misha.kafka.avro.serializers;

import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.stream.IntStream;

import static org.misha.Utils.*;

//pushes schema into registry
public class GenericTracerProducer {

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.serializer", KafkaAvroSerializer.class.getName());
        props.put("value.serializer", KafkaAvroSerializer.class.getName());
        props.put("schema.registry.url", "http://localhost:8081");
        try (KafkaProducer<String, GenericRecord> producer = new KafkaProducer<>(props)) {
            Schema.Parser parser = new Schema.Parser();
            Schema schema = parser.parse(readResource("trace.avsc"));
            IntStream.range(0, 1000).mapToObj(t -> {
                GenericData.Record traceData = new GenericData.Record(schema);
                traceData.put("id", "trackAvroGenericRecord");
                traceData.put("latitude", round(51.51101 + t * (51.50757 - 51.51101) / 1000).doubleValue());
                traceData.put("longitude", round(-0.07620 + t * (-0.12387 - (-0.07620)) / 1000).doubleValue());
                return traceData;
            }).forEach(traceData -> {
                ProducerRecord<String, GenericRecord> record
                        = new ProducerRecord<>("TraceAvroGenericTopic", "Trace_Track_0", traceData);
                try {
                    RecordMetadata recordMetadata = producer.send(record, (metadata, exception) -> {
                        if (exception != null) {
                            throw new RuntimeException(writeJson(record));
                        }
                        System.err.println("Successfully sent: " + traceData + "\nmeta:" + metadata);
                    }).get();
                    System.err.println(recordMetadata);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
