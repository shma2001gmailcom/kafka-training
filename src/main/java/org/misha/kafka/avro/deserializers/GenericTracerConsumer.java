package org.misha.kafka.avro.deserializers;

import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

//reads schema from registry
public class GenericTracerConsumer {
    public static void main(String[] args) {
        final AtomicLong lastTimestamp = new AtomicLong();
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.deserializer", KafkaAvroDeserializer.class.getName());
        props.put("value.deserializer", KafkaAvroDeserializer.class.getName());
        props.put("schema.registry.url", "http://localhost:8081");
        props.put("group.id", "TraceAvroGenericTopicId");
        try (KafkaConsumer<String, GenericRecord> consumer = new KafkaConsumer<>(props)) {
            consumer.subscribe(Collections.singletonList("TraceAvroGenericTopic"));
            while (true) {
                ConsumerRecords<String, GenericRecord> traceData = consumer.poll(Duration.ZERO);
                for (ConsumerRecord<String, GenericRecord> traceRecord : traceData) {
                    if (lastTimestamp.get() < traceRecord.timestamp()) {
                        lastTimestamp.set(traceRecord.timestamp());
                        GenericRecord value = traceRecord.value();
                        System.out.printf("\n=======================" +
                                        "\nrecord: headers=%s" +
                                        "\nTrack is=%s" +
                                        "\npartition=%s" +
                                        "\ntopic=%s" +
                                        "\ntimestamp=%s" +
                                        "\nlatitude=%s" +
                                        "\nlongitude=%s",
                                traceRecord.headers(),
                                traceRecord.key(),
                                traceRecord.partition(),
                                traceRecord.topic(),
                                traceRecord.timestamp(),
                                value.get("latitude"),
                                value.get("longitude"));
                    }
                }
            }
        }
    }
}
