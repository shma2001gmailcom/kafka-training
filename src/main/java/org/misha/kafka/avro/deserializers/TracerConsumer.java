package org.misha.kafka.avro.deserializers;

import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.misha.kafka.avro.Trace;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

//reads schema from registry
public class TracerConsumer {
    public static void main(String[] args) {
        final AtomicLong lastTimestamp = new AtomicLong();
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.deserializer", KafkaAvroDeserializer.class.getName());
        props.put("value.deserializer", KafkaAvroDeserializer.class.getName());
        props.put("schema.registry.url", "http://localhost:8081");
        props.put("specific.avro.reader", "true");
        props.put("group.id", "TraceAvroTopicId");
        try (KafkaConsumer<String, Trace> consumer = new KafkaConsumer<>(props)) {
            consumer.subscribe(Collections.singletonList("TraceAvroTopic"));
            while (true) {
                ConsumerRecords<String, Trace> records = consumer.poll(Duration.ZERO);
                for (ConsumerRecord<String, Trace> record : records) {
                    if (lastTimestamp.get() < record.timestamp()) {
                        lastTimestamp.set(record.timestamp());
                        System.out.printf("\n=======================" +
                                        "\nrecord: headers=%s" +
                                        "\nTrack is=%s" +
                                        "\npartition=%s" +
                                        "\ntopic=%s" +
                                        "\ntimestamp=%s" +
                                        "\nlatitude=%s" +
                                        "\nlongitude=%s",
                                record.headers(),
                                record.key(),
                                record.partition(),
                                record.topic(),
                                record.timestamp(),
                                record.value().getLatitude(),
                                record.value().getLongitude());
                    }
                }
            }
        }
    }
}
