
==========avro package requires confluent=====================
export CONFLUENT_HOME=/home/mshevelin/confluent-7.5.0
export PATH=$PATH:$CONFLUENT_HOME/bin
confluent local services start
confluent local services stop

===========create partitioned topic==========
kafka-topics --create --bootstrap-server localhost:9092 --replication-factor 1 -- partitions 10 --topic TracingPartitionedTopic
===========describe topic====================
kafka-topics --describe --bootstrap-server localhost:9092 --topic TracingPartitionedTopic


