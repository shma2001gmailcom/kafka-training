package org.misha;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.misha.partitioned.PlacePartitioner;
import org.misha.serialization.TraceSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.stream.IntStream;

import static org.misha.Utils.round;
import static org.misha.Utils.writeJson;

public class TracerProducer {
    private static final Logger log = LoggerFactory.getLogger(TracerProducer.class);

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, TraceSerializer.class.getName());
        props.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, PlacePartitioner.class.getName());
        try (KafkaProducer<String, Trace> producer = new KafkaProducer<>(props)) {
            IntStream.range(0, 1000).mapToObj(t -> new Trace("TRACK_0", round(51.51101 + t * (51.50757 - 51.51101) / 1000),
                    round(-0.07620 + t * (-0.12387 - (-0.07620)) / 1000))).forEach(trace -> {
                ProducerRecord<String, Trace> record = new ProducerRecord<>("TracingPartitionedTopic", "Trace_Track_0", trace);
                try {
                    RecordMetadata recordMetadata = producer.send(record, (metadata, exception) -> {
                        if (exception != null) {
                            throw new RuntimeException(writeJson(record) + "\n" + exception);
                        }
                        log.error("Successfully sent: " + trace + "\nmeta:" + metadata);
                    }).get();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
