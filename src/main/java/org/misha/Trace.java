package org.misha;

import java.math.BigDecimal;

import static org.misha.Utils.writeJson;

public class Trace {
    private String id;
    private BigDecimal latitude;
    private BigDecimal longitude;

    public Trace() {
    }

    public Trace(String id, BigDecimal lat, BigDecimal lon) {
        this.id = id;
        latitude = lat;
        longitude = lon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return writeJson(this);
    }

    public boolean isInSquare(double lat, double lon, double length) {
        return latitude.compareTo(BigDecimal.valueOf(lat)) <= 0
                && latitude.compareTo(BigDecimal.valueOf(lat - length)) >= 0
                && longitude.compareTo(BigDecimal.valueOf(lon)) <= 0
                && longitude.compareTo(BigDecimal.valueOf(lon - length)) >= 0;
    }
}
