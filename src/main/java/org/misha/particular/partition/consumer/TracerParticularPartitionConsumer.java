package org.misha.particular.partition.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.misha.Trace;
import org.misha.serialization.TraceDeserializer;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

import static org.apache.kafka.clients.consumer.ConsumerConfig.*;

public class TracerParticularPartitionConsumer {
    public static void main(String[] args) {
        final AtomicLong lastTimestamp = new AtomicLong();
        Properties props = new Properties();
        props.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(KEY_DESERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.StringDeserializer.class.getName());
        props.put(VALUE_DESERIALIZER_CLASS_CONFIG, TraceDeserializer.class.getName());
        props.put(AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(GROUP_ID_CONFIG, "SimpleConsumerGroup");//single consumer in this group
        try (KafkaConsumer<String, Trace> consumer = new KafkaConsumer<>(props)) {
            List<PartitionInfo> partitionInfoList = consumer.partitionsFor("SimpleConsumerTopic");
            List<TopicPartition> partitions = new ArrayList<>();
            partitionInfoList.forEach(info -> {
                System.out.println(info.partition());
                partitions.add(new TopicPartition("SimpleConsumerTopic", info.partition()));
            });
            consumer.assign(partitions);
            while (true) {
                ConsumerRecords<String, Trace> records = consumer.poll(Duration.ZERO);
                for (ConsumerRecord<String, Trace> record : records) {
                    if (lastTimestamp.get() < record.timestamp()) {
                        lastTimestamp.set(record.timestamp());
                        System.out.printf("\n=======================" +
                                        "\nrecord: headers=%s" +
                                        "\nTrack is=%s" +
                                        "\nPARTITION=%s" +
                                        "\ntopic=%s" +
                                        "\ntimestamp=%s" +
                                        "\nlatitude=%s" +
                                        "\nlongitude=%s",
                                record.headers(),
                                record.key(),
                                record.partition(),
                                record.topic(),
                                record.timestamp(),
                                record.value().getLatitude(),
                                record.value().getLongitude());
                    }
                    consumer.commitAsync();
                }
            }
        }
    }
}
