package org.misha.serialization;

import org.apache.kafka.common.serialization.Serializer;
import org.misha.Trace;

import java.nio.charset.StandardCharsets;

public class TraceSerializer implements Serializer<Trace> {

    @Override
    public byte[] serialize(String topic, Trace trace) {
        try {
            return trace.toString().getBytes(StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
