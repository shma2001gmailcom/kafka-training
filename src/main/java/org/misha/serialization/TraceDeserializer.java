package org.misha.serialization;

import org.apache.kafka.common.serialization.Deserializer;
import org.misha.Trace;

import static org.misha.Utils.fromBytes;

public class TraceDeserializer implements Deserializer<Trace> {
    @Override
    public Trace deserialize(String topic, byte[] data) {
        return fromBytes(data);
    }
}
