package org.misha.producer.properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.misha.Trace;
import org.misha.partitioned.PlacePartitioner;
import org.misha.serialization.TraceSerializer;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.stream.IntStream;

import static org.apache.kafka.clients.producer.ProducerConfig.*;
import static org.misha.Utils.round;
import static org.misha.Utils.writeJson;

public class TracerProducer {

    public static void main(String[] args) {
        Properties props = makeProperties();
        try (KafkaProducer<String, Trace> producer = new KafkaProducer<>(props)) {
            IntStream.range(0, 1000).mapToObj(t -> new Trace("TRACK_0", round(51.51101 + t * (51.50757 - 51.51101) / 1000),
                    round(-0.07620 + t * (-0.12387 - (-0.07620)) / 1000))).forEach(trace -> {
                ProducerRecord<String, Trace> record = new ProducerRecord<>("TracingPartitionedTopic", "Trace_Track_0", trace);
                try {
                    RecordMetadata recordMetadata = producer.send(record, (metadata, exception) -> {
                        if (exception != null) {
                            throw new RuntimeException(writeJson(record) + "\n" + exception);
                        }
                        System.err.println("Successfully sent: " + trace + "\nmeta:" + metadata);
                    }).get();
                    System.err.printf("record metadata: %s", recordMetadata);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    throw new RuntimeException(e);
                } catch (ExecutionException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static Properties makeProperties() {
        Properties props = new Properties();
        props.put(BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(KEY_SERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.StringSerializer.class.getName());
        props.put(VALUE_SERIALIZER_CLASS_CONFIG, TraceSerializer.class.getName());
        props.put(ACKS_CONFIG, "all");
        props.put(BUFFER_MEMORY_CONFIG, "1024");
        props.put(COMPRESSION_TYPE_CONFIG, "snappy");//gzip = higher compression ratio, snappy = lowest latency
        props.put(RETRIES_CONFIG, "1");//0 means "At mostOnce"
        props.put(RETRY_BACKOFF_MS_CONFIG, "1000");
        props.put(BATCH_SIZE_CONFIG, 10240);
        props.put(LINGER_MS_CONFIG, 5);
        props.put(REQUEST_TIMEOUT_MS_CONFIG, 100);//default
        props.put(ENABLE_IDEMPOTENCE_CONFIG, "true");
        props.put(PARTITIONER_CLASS_CONFIG, PlacePartitioner.class.getName());
        return props;
    }
}
