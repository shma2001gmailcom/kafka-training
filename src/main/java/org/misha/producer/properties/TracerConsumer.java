package org.misha.producer.properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.misha.Trace;
import org.misha.serialization.TraceDeserializer;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

public class TracerConsumer {
    public static void main(String[] args) {
        final AtomicLong lastTimestamp = new AtomicLong();
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.deserializer", org.apache.kafka.common.serialization.StringDeserializer.class.getName());
        props.put("value.deserializer", TraceDeserializer.class.getName());
        props.put("group.id", "TraceTopicId");
        try (KafkaConsumer<String, Trace> consumer = new KafkaConsumer<>(props)) {
            consumer.subscribe(Collections.singletonList("TracingPartitionedTopic"));
            while (true) {
                ConsumerRecords<String, Trace> records = consumer.poll(Duration.ZERO);
                for (ConsumerRecord<String, Trace> record : records) {
                    if (lastTimestamp.get() < record.timestamp()) {
                        lastTimestamp.set(record.timestamp());
                        System.out.printf("\n=======================" +
                                        "\nrecord: headers=%s" +
                                        "\nTrack is=%s" +
                                        "\nPARTITION=%s" +
                                        "\ntopic=%s" +
                                        "\ntimestamp=%s" +
                                        "\nlatitude=%s" +
                                        "\nlongitude=%s",
                                record.headers(),
                                record.key(),
                                record.partition(),
                                record.topic(),
                                record.timestamp(),
                                record.value().getLatitude(),
                                record.value().getLongitude());
                    }
                }
            }
        }
    }
}
