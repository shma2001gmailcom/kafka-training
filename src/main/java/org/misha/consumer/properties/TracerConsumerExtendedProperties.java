package org.misha.consumer.properties;

import org.apache.kafka.clients.consumer.*;
import org.misha.Trace;
import org.misha.serialization.TraceDeserializer;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicLong;

public class TracerConsumerExtendedProperties {
    public static void main(String[] args) {
        final AtomicLong lastTimestamp = new AtomicLong();
        Properties props = makeProperties();
        try (KafkaConsumer<String, Trace> consumer = new KafkaConsumer<>(props)) {
            consumer.subscribe(Collections.singletonList("TracingPartitionedTopic"));
            while (true) {
                ConsumerRecords<String, Trace> records = consumer.poll(Duration.ZERO);
                for (ConsumerRecord<String, Trace> record : records) {
                    if (lastTimestamp.get() < record.timestamp()) {
                        lastTimestamp.set(record.timestamp());
                        System.out.printf("\n=======================" +
                                        "\nrecord: headers=%s" +
                                        "\nTrack is=%s" +
                                        "\nPARTITION=%s" +
                                        "\ntopic=%s" +
                                        "\ntimestamp=%s" +
                                        "\nlatitude=%s" +
                                        "\nlongitude=%s",
                                record.headers(),
                                record.key(),
                                record.partition(),
                                record.topic(),
                                record.timestamp(),
                                record.value().getLatitude(),
                                record.value().getLongitude());
                    }
                }
            }
        }
    }

    private static Properties makeProperties() {
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, org.apache.kafka.common.serialization.StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, TraceDeserializer.class.getName());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "TraceTopicId");
        props.put(ConsumerConfig.FETCH_MIN_BYTES_CONFIG, "10240");
        props.put(ConsumerConfig.FETCH_MAX_WAIT_MS_CONFIG, "200");
        props.put(ConsumerConfig.HEARTBEAT_INTERVAL_MS_CONFIG, "1000");
        props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, "30000");//3 times more
        props.put(ConsumerConfig.MAX_PARTITION_FETCH_BYTES_CONFIG, "1000000");//default
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");//earliest reads all records from the partition
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, "TracerConsumerExtendedProperties");
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "100");
        props.put(ConsumerConfig.PARTITION_ASSIGNMENT_STRATEGY_CONFIG, RangeAssignor.class.getName()); //or RoundRobinAssignor
        return props;
    }
}
